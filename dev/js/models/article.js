

App.Models.Article = Backbone.Model.extend({
    localStorage: new Backbone.LocalStorage("ArticleModel"),

    defaults: {
        id: 1,
        siteUrl: '',
        keyword: '',
        seoTitle: '',
        metaDescription: '',
        articleTitle: '',
        articleContent: ''
    },

    initialize: function () {
    },

    getSeoTitle: function(url, callback) {
        if (url) {
            h.getPageTitle(url, callback);
        }
    },

    validation: {
        siteUrl: {
            required: true,
            msg: 'Direccion de la pagina web necesaria, si no existe, puedes poner una falsa'
        },
        keyword: {
            required: true,
            msg: 'Palabra Clave Necesaria'
        },
        seoTitle: {
            required: true,
            msg: 'Titulo SEO Requerido'
        },
        // metaDescription: {
        //     required: true,
        //     msg: 'Meta Descripcion Necesaria'
        // },
        articleTitle: {
            required: true,
            msg: 'Titulo del Articulo Necesario'
        },
        articleContent: [{
            required: true,
            msg: "Contenido del Articulo es Requerido, sin el no podemos calificar"

        },{
            minLength: 300,
            msg: 'Minimo de Caracteres en el Cuerpo del Articulo es de 300'
        }]
    }
});


