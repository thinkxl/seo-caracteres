App.Models.Score = Backbone.Model.extend({
    defaults: {
        scoreIcon: '',
        scoreTitle: '',
        scoreMsg: '',
        scoreNum: ''
    }
});

