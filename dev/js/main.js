/*
 * Global 
 */
window.App = {
        Models: {},
        Collections: {},
        Views: {},
        Routers: {},
        init: function() {
            'use strict';
        }
    },
    template = function(id) {
        return _.template($('#' + id).html());
        console.log('template: ' + id);
    };

/*
 * Start App!
 */
$(document).ready(function() {
    'use strict';

    CKEDITOR.on('instanceReady', function() {
        App.init();  

        window.article = new App.Models.Article();
        window.articleView = new App.Views.Article({model: article});

        article.on({
            'invalid': function(model, error) {
                var newErrors = new App.Collections.Errors(),
                    newErrorsView = new App.Views.Errors({ collection: newErrors });

                _.each(error, function(value, key) {
                    newErrors.add({ msg: value });
                });
            }
        });
   });
});
