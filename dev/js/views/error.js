App.Views.Error = Backbone.View.extend({
    tagName: 'li',
    className: 'error',

    template: template('error-template'),

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));

        return this;
    }
});
