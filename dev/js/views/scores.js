App.Views.Scores = Backbone.View.extend({
    el: '.score-list',

    initialize: function () {
        this.collection.on('add', this.addOne, this);
        this.render();
    },

    render: function () {
        this.resetAndShow();

        // render this view
        this.collection.each(this.addOne, this);

        return this;
    },

    addOne: function(model) {
        var score = new App.Views.Score({ model: model });
        this.$el.append(score.render().el);
    },

    resetAndShow: function() {
        this.$el.html('');

        $('.score').show();
        $('.errors').hide();

        h.spinner(3, '.snippet', h.stopScroll, h.allowScroll);
    }
});
