App.Views.Snippet = Backbone.View.extend({
    el: '.snippet-review',

    seoTitle: '',
    siteUrl: '',
    keyword: '',
    metaDescription: '',
    cleanUrl: '',

    initialize: function() {
        
        this.getData();
        this.setTitle(this.seoTitle, this.cleanUrl , this.keyword);
        this.setUrl(this.cleanUrl, this.keyword);
        this.setDescription(this.keyword, this.metaDescription);
    },

    shortString: function(limit, string) {
        var goodString;
        string.length > limit ? goodString = string.substr(0, limit - 3) + "..." : goodString = string;
        console.log("goodString: " + goodString);
        return goodString;
    },

    getData: function() {
        this.seoTitle = this.shortString(68, this.model.get('seoTitle'));
        this.siteUrl = this.shortString(75, this.model.get('siteUrl'));
        this.keyword = this.model.get('keyword');
        this.metaDescription = this.model.get('metaDescription');
        this.cleanUrl = this.shortString(75, this.getCleanTitle(this.siteUrl, this.seoTitle));
    },

    getCleanTitle: function(url, title) {
        var slugified = h.slugify(title.split('-')[0]),
            cleanTitle = slugified.replace(/[^-a-zA-Z0-9]/g, '');

        return url + '/' + cleanTitle;
    },

    setTitle: function(title, url, keyword) {
        var $ele = this.$el.find('.snippet-review-title');

        $ele.attr('href', url).html(h.getTextBold(title, keyword));
    },

    setUrl: function(url, keyword) {
        var $ele = this.$el.find('.snippet-review-url');

        $ele.attr('href', url).html(h.getTextBold(url, h.slugify(keyword)));
    },

    setDescription: function(keyword, metaDescription) {
        var $ele = this.$el.find('.snippet-review-desc');

        $ele.html(h.getTextBold(metaDescription, keyword));
    }
});
