App.Views.Article = Backbone.View.extend({
    el: '.js-article-data',

    events: {
        'blur .js-siteUrl': 'saveUrl',
        'focus .js-seoTitle': 'seoTitleCharsLeft',
        'focus .js-metaDescription': 'metaDescriptionCharsLeft',
        'click .js-seoCheck': 'checkSEO'
    },

    initialize: function () {
        // backbone validation plugin binding
        Backbone.Validation.bind(this);

        this.render();
    },

    render: function () {
        this.getData();
    },

    /*
     * Update characters left functions 
     */
    metaDescriptionCharsLeft: function(e) {
        this.countFn($(e.target), 156);
    },

    seoTitleCharsLeft: function(e) {
        this.countFn($(e.target), 70);
    },

    /*
     * Count Characters Functions
     */
    countFn: function(el, limit) {
        var that = this;

         el.on({
            'keyup': function() {
                that.countChars(el, limit);
            },
            'blur': function() {
                el.off('keyup'); 
            }
        });
    },

    countChars: function(el, limit) {
            var counter = el.next().find('.chars-left'),
            num = limit - el.val().length;

            num < 10 ? counter.removeClass('text-green').addClass('text-red') : counter.removeClass('text-red').addClass('text-green');
            counter.text(num);
    },

    updateCharsLeft: function() {
        var that = this;
        
        that.countChars(that.$el.find('.js-seoTitle'), 70);
        that.countChars(that.$el.find('.js-metaDescription'), 156);
    },

    /*
     * get the title of the url using YQL
     */
    saveUrl: function(e) {
        this.$el.find('.js-seoTitle').attr('disabled', true);

        var url = this.$el.find('.js-siteUrl').val();
        this.model.set({'siteUrl': url});
        this.getSeoTitle(this.model.get('siteUrl'));
    },

    getSeoTitle: function(url) {
        var that = this,
            seoTitle = that.$el.find('.js-seoTitle'),
            articleTitle = that.$el.find('.js-articleTitle');
        
        this.model.getSeoTitle(url, function(data) {
            seoTitle.val(articleTitle.val() + " - " + data).attr('disabled', false);

            // change characters left
            that.updateCharsLeft();
        });
    },

    checkSEO: function(e) {
        e.preventDefault();

        this.saveData();

        if (this.model.isValid(true)) {
            this.performActions();
            new App.Views.Snippet({ model: this.model });
        } else {
            console.log('Invalid');
        }
    },

    /*
     * Save data into LocalStorage
     */
    saveData: function() {
        var els = this.$el.find('[data-el]');

        // each element that holds data
        // get value and set model attr that
        // matches same name
        _.each(els, function(value, key) {
            var attr = els.eq(key).attr('data-el'),
                val = els.eq(key).val();

            this.model.set(attr,  val);
        }, this);

        // special case for CKEDITOR
        this.model.set({ 'articleContent': editor.getArticleHTML() });
        this.model.save({ validate: true });
    },

    /*
     * Get data from LocalStorage
     */
    getData: function() {
        var els = this.$el.find('[data-el]');

        this.model.fetch();

        // go trough all elements that holds data
        // and get the model attr that matches the name
        _.each(els, function(value, key) {
            var name = els.eq(key).attr('data-el'),
                attr = this.model.get(name);
            els.eq(key).val(attr);
        }, this);
            
        // using this because is getting the data from CKEDITOR 
        // so is a special case
        editor.insertHtml(this.model.get('articleContent'));
        
        // update charecters left
        this.updateCharsLeft();
    },

    showErrors: function(errors) {
        
    },

    performActions: function() {
        var content = editor.getArticleText(),
            html = editor.getArticleHTML(),
            articleTitle = this.model.get('articleTitle') || '',
            keyword = this.model.get('keyword'),
            metaDescription = this.model.get('metaDescription'),
            totalWords = h.getTotalWords(content),
            totalSentences = h.getTotalSentences(content),
            totalSyllables = h.getSyllables(content),
            totalImages = h.getImagesLength(html),
            subHeadings = new RegExp('<h[1-6]>.*' + keyword + '.*<\/h[1-6]>', 'gi'),
            firstParagraph = h.getFirstParagraph(html).replace(/<[^<]+?>/g, '');

        console.log('keyword density: ' + h.getKeywordDensity(content, keyword));
        console.log('flesch reading ease: ' + h.getFleschReadingEase(totalWords, totalSentences, totalSyllables));
        console.log('total images: ' + totalImages);
        console.log('keyword on title: ' + h.getKeywordsOnString(articleTitle, keyword));
        console.log('title length: ' + articleTitle.length);
        console.log('there are sub headers: ' + h.subHeadings(html, keyword).exist());
        console.log('keyword on sub headings: ' + h.subHeadings(html, keyword).keywords());
        console.log('keyword on first paragraph: ' + h.getKeywordsOnString(firstParagraph, keyword));
        console.log('keyword on meta description: ' + h.getKeywordsOnString(metaDescription, keyword));
        console.log('length of content :' + content.length);
        console.log('there are meta description: ' + (metaDescription.length > 0));

        // keyword density
        var scores = new App.Views.Scores({ collection: new App.Collections.Scores }),
            keywordDensity = h.getKeywordDensity(content, keyword),
            fleschReadingEase = h.getFleschReadingEase(totalWords, totalSentences, totalSyllables),
            keywordOnTitle = h.getKeywordsOnString(articleTitle, keyword),
            keywordOnSubheadings = h.subHeadings(html, keyword),
            keywordOnFirstParagraph = h.getKeywordsOnString(firstParagraph, keyword),
            keywordOnMetaDescription = h.getKeywordsOnString(metaDescription, keyword);

        var kwMsg = "La Densidad de la Palabra Clave es de: ";

        if (keywordDensity < 1.99) {
            scores.collection.add({ 
                scoreIcon: 'minus-square',
                scoreTitle: kwMsg,
                scoreNum: keywordDensity,
                scoreMsg: h.scores.keywordDensity.one
            });
        }

        if (keywordDensity > 1.99 && keywordDensity < 5) {
            scores.collection.add({ 
                scoreIcon: 'check-square',
                scoreTitle: kwMsg,
                scoreNum: keywordDensity,
                scoreMsg: h.scores.keywordDensity.twe
            });
        }

        if (keywordDensity > 5) {
            scores.collection.add({ 
                scoreIcon: 'minus-square',
                scoreTitle: kwMsg,
                scoreNum: keywordDensity,
                scoreMsg: h.scores.keywordDensity.three
            });
        }

        // meta description

        if (! metaDescription.length > 0) {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreMsg: h.scores.metaDescription.one
            });
        }

        // flesch reading ease
        var fre = fleschReadingEase,
            freMsg = 'La calificacion del <a target="_blank" href="http://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests">Flesch Reading Ease</a> es de: ';

        if (fre < 100 && fre > 90) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: freMsg,
                scoreNum: fre,
                scoreMsg: h.scores.fleschReadingEase.one
            });
        }

        if (fre > 30 && fre < 90) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: freMsg,
                scoreNum: fre,
                scoreMsg: h.scores.fleschReadingEase.two
            });
        }

        if (fre < 30) {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: freMsg,
                scoreNum: fre,
                scoreMsg: h.scores.fleschReadingEase.three
            });
        }

        // images 
        var imgs = totalImages;

        if (totalImages) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: h.scores.images.one
            });
        } else {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.images.two
            });
        }

        // keyword on title
        var kwOt = keywordOnTitle;

        if (kwOt) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: h.scores.articleTitle.one
            });
        } else {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.articleTitle.two
            });
        }

        // title length four five
        var tl = articleTitle.length;

        if (tl < 30) {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.articleTitle.four
            });
        }

        if (tl > 70) {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.articleTitle.five
            });
        }

        if (tl < 70 && tl > 30) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: h.scores.articleTitle.three
            });
        }

        // keyword on subheadings
        var kwOsh = keywordOnSubheadings;

        if (kwOsh.exist()) {
            if (kwOsh.keywords() > 0) {
                scores.collection.add({
                    scoreIcon: 'check-square',
                    scoreTitle: h.scores.keywordOnSubheadings.one
                });
            } else {
                scores.collection.add({
                    scoreIcon: 'minus-square',
                    scoreTitle: h.scores.keywordOnSubheadings.two
                });
            }           
        } else {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.keywordOnSubheadings.three
            })
        }

        // keyword on first paragraph
        var kwOfp = keywordOnFirstParagraph;

        if (kwOfp) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: h.scores.keywordOnfirstParagraph.one
            });
        } else {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.keywordOnfirstParagraph.two
            });
        }

        // keyword on meta description
        kwOmd = keywordOnMetaDescription;

        if (kwOmd) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: h.scores.keywordOnMetaDescription.one
            });
        } else {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: h.scores.keywordOnMetaDescription.two
            });
        }

        // content length
        var cle = content.length
            cleMsg = 'Cantidad de Palabras en el Texto: ';

        if (cle > 300) {
            scores.collection.add({
                scoreIcon: 'check-square',
                scoreTitle: cleMsg,
                scoreNum: cle,
                scoreMsg: h.scores.wordsLength.one
            });
        } else {
            scores.collection.add({
                scoreIcon: 'minus-square',
                scoreTitle: cleMsg,
                scoreNum: cle,
                scoreMsg: h.scores.wordsLength.two
            });
        }
    }
});
