App.Views.Errors = Backbone.View.extend({
    el: '.errors-list',

    initialize: function () {
        this.resetAndShow();
        this.collection.on('add', this.addOne, this);
        this.render();
    },

    render: function (model) {
        this.collection.each(this.addOne, this);

        return this;
    },

    addOne: function(model) {
        var error = new App.Views.Error({ model: model });

        this.$el.append(error.render().el);
    },

    resetAndShow: function() {
        this.$el.html('');

        $('.errors').show();
        $('.score').hide();

        h.spinner(3, '.errors', h.stopScroll, h.allowScroll);
    }
});
