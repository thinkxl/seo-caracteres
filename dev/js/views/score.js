App.Views.Score = Backbone.View.extend({
    tagName: 'li',
    className: 'score-item',

    template: template('score-template'),

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));    

        return this;
    }
});

