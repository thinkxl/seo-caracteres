// validation of fields
// snippet preview autofill - DONE
// add other fields to keywordDensity feature, keyword density should be 1% - 4.5% including url and meta description
// make bold keyword on all fields
// check if keywords is in the begginning of title
// validate if bodyText has more than 300 chars

# things to qualify

The page title contains keyword / phrase, but it does not appear at the beginning; try and move it to the beginning.
No subheading tags (like an H2) appear in the copy.
The copy scores 65.5 in the Flesch Reading Ease test, which is considered OK to read.
**The keyword / phrase appears in the URL for this page.**
**The keyword density is 1.70%, which is great, the keyword was found 7 times.**
The page title is more than 40 characters and less than the recommended 70 character limit.
The meta description contains the primary keyword / phrase.
There are 414 words contained in the body copy, this is more than the 300 word recommended minimum.
The keyword appears in the first paragraph of the copy.

