unction($) {
    console.log('hi');
    // helpers
    $.fn.countChar = function() {
        var that = this,
            limit = this.attr('data-limit'),
            counter = this.next().find('.chars-left'),
            num = 0;

        that.on('keyup', function() {
            num = limit-that.val().length;
            num < 10 ? counter.removeClass('text-green').addClass('text-red') : counter.removeClass('text-red').addClass('text-green');
            counter.text(num);
        });

        return this;
    };

    // app
    var s,
        app = {
            //settings
            s: {
                webSite: $('.seo-data-website'),
                keyword: $('.seo-data-keyword'),
                seoTitle: $('.seo-data-title'),
                metaDesc: $('.seo-data-meta-description'),
                testResults: $('.test').find('ul'),

                button: $('.check-button'),
                scoreTemplate: Handlebars.compile($('#score-template').html()),

                snippetTitle: $('.snippet-review-title'),
                snippetUrl: $('.snippet-review-url'),
                snippetDesc: $('.snippet-review-desc')
            },

            init: function() {
                s = this.s;
                this.bind();
            },
        
            bind: function() {
                s.seoTitle.countChar();
                s.metaDesc.countChar();

                s.button.on('click', function(e) {
                    e.preventDefault();

                    app.snippetPreview();
                    app.getResultsOfTest();

                    // only for testing purpose
                    console.log('flesh reading ease score: ' + app.getFleschReadingEaseScore(app.getBodyText()));
                    app.keywordDensity(app.getBodyText(), s.keyword.val());
                    console.log('keyword on string: ' + app.keywordOnString(app.getBodyText(), s.keyword.val()));
                });
            },

            // this get the plain text of the WYSIWYG Editor
            getBodyText: function() {
                var data = CKEDITOR.instances.editor1.document.getBody().getText();
                return data;
            },

            // this get the HTML of the WYSIWYG Editor
            getBodyHTML: function() {
                var data = CKEDITOR.instances.editor1.getData();
                return data;
            },

            // count the syllables on of a string
            getSyllables: function(text) {
                if (text.length <= 3) {
                    return 1;
                } else {
                    return text.replace(/^y/, '').match(/[aeiou]{1,2}/g).length*0.92;                    
                }
            },

            // formula to calculate the Flesch Reading Ease
            fleschReadingEase: function(totalWords, totalSentences, totalSyllables) {
                var score = 206.835 - (1.015*totalWords)/totalSentences - (84.6*totalSyllables)/totalWords;
                return score.toFixed(2);
            },

            // this function get the data needed to return the Flesch Reading Ease
            getFleschReadingEaseScore: function(bodyText) {
                var endOfParagraph = bodyText.split('&nbsp;').length,
                    totalWords = bodyText.replace(/\s\./g, '.').replace(/\s\,/g, ',').split(' ').length,
                    totalSentences = function() { 
                        if (bodyText.split('.').length > 1) { 
                            return bodyText.match(/\.\s/g).length + 1;
                        } else {
                            return 1;
                        }
                    },
                    totalSyllables = app.getSyllables(bodyText);

                console.log('Characteres: ', bodyText.length);
                console.log('totalWords: ', totalWords);
                console.log('totalSentences: ', totalSentences());
                console.log('totalSyllables: ', totalSyllables);

                return app.fleschReadingEase(totalWords, totalSentences(), totalSyllables);
            },

            // keyword density, return the keyword density of a string, requires a string and the keyword
            keywordDensity: function(bodyText, keyword) {
                // todo
                // get keywords from title and metatags, or all inputs to get 
                // more accurate keywords density
                
                var tkn = bodyText.toLowerCase().replace(/\s\./g, '.').replace(/\s\,/g, '.').split(' ').length,
                    nkr = app.keywordOnString(bodyText, keyword);

                console.log('kwc density: ' + ((nkr/tkn)*100));

                return ((nkr/tkn)*100).toFixed(2);
            },

            // this return how many times is a keyword on a string
            keywordOnString: function(text, keyword) {
                var kw = new RegExp(keyword, 'g');
                return text.toLowerCase().match(kw).length;
            },

            // get snippet preview like google's
            snippetPreview: function() {
                var makeTitle = function(url, seotitle) {
                        var title = app.slugify(seotitle.split('-')[0]),
                            cleanTitle = title.replace(/[^-a-zA-Z0-9]/g, '');

                        return url + '/' + cleanTitle;
                    },
                    keywordSlugified = app.slugify(s.keyword.val()),     // keyword-like-this
                    keywordSimple = s.keyword.val(),                     // Keyword Like This
                    url = makeTitle(s.webSite.val(), s.seoTitle.val()); 

                // insert data into each element
                s.snippetTitle
                    .attr('href', url)
                    // make the keyword bold, then capitalize the title and the keyword
                    .html(app.makeTitleKeywordBold(app.capitalize(s.seoTitle.val()), app.capitalize(keywordSimple)));

                s.snippetUrl
                    .attr('href', url)
                    // make the keyword bold, then slugify the keyword to match the url style
                    .html(app.makeKeywordBold(url, app.slugify(s.keyword.val())));

                s.snippetDesc
                    // make the keyword bold
                    .html(app.makeKeywordBold(s.metaDesc.val(), keywordSimple));
            },

            // make the keyword bold on the snippet view
            makeKeywordBold: function(str, keyword) {
                var result = str.replace(keyword, '<strong>' + keyword + '</strong>');
                return result;
            },

            // capitalise any string making the fisrt letter uppercase
            capitalize: function(str) {
                return str.replace(/^(.)|\s(.)/g, function($1) { return $1.toUpperCase() });
            },  

            // convert this: 'Something Like this' into this 'something-like-this'
            // also check if the last char is '-' and if yes, delete it
            slugify: function(str) {
                var slugified = str.toLowerCase().replace(/\s/g, '-');

                if (slugified.substr(-1) === '-') {
                    slugified = slugified.substr(0, slugified.length -1);
                }

                return slugified;
            },

            getResultsOfTest: function() {
                // KEYWORD DENSITY 
                //make an object with elements
                var k = {
                    score: 0,
                    txt:  'Densidad de la Palabra Clave es de: ',
                    comment: '',
                    icon: ''
                };

                //get keyword density
                k.score = app.keywordDensity(app.getBodyText(), s.keyword.val());

                if (k.score < 1) { 
                    k.comment = 'Es algo bajo, usar la palabra clave algunas veces mas, ayudaria';
                    k.icon = 'circle';
                } 

                if (k.score > 1 && k.score < 4.5) { 
                    k.comment = 'Excelente densidad de la palabra clave';
                    k.icon = 'check-circle'
                }

                if (k.score > 4.5) {
                    k.comment = 'Exceso del uso de la palabra clave, los motores de busqueda podrian multarte';
                    k.icon = 'cicle';
                }

                s.testResults.append(s.scoreTemplate(k));

                // DIFFICULTY TO READ
                k.txt = 'Calificacion que mide la Dificultad para leer el texto: ';
                k.score = app.getFleschReadingEaseScore(app.getBodyText());

                if (k.score > 90) {
                    k.comment = 'Facil de leer';
                    k.icon = 'check-circle';
                }

                if (k.score > 60 && k.score < 70) {
                    k.comment = 'Mas o Menos';
                    k.icon = 'circle';
                }

                if (k.score > 30 && k.score < 60) {
                    k.comment = 'Dififil de leer';
                    k.icon = 'circle';
                }

                if (k.score < 30) {
                    k.comment = 'Pesimo!';
                    k.icon = 'circle';
                }

                s.testResults.append(s.scoreTemplate(k));
            }
        };

    app.init();

})(jQuery);



// totalWords = numero de palabras
// totalSentences = cada punto es una sentence
// syllables = getSyllables function





