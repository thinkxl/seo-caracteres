var h = {

    smoothScroll: function(id) {
        var target = $(id).offset().top - 60;
        $('html, body').animate({scrollTop:target}, 500);
    },

    stopScroll:function() {
        $('body').on('mousewheel touchmove', function(e) {
            e.preventDefault();
            console.log('scrolling');
        });
    },

    allowScroll: function() {
        $('body').off('mousewheel touchmove');
    },

    spinner: function(time, target, before, after) {
        before();
        var spiner,
            d = document,
            $body = $('body'),
            bg = d.createElement('div'),
            smoothScroll = this.smoothScroll;
            opts = { color: '#fff', speed: 2.2, shadow: true };

            bg.id = 'background';

        $body
            .prepend(bg)
            .find('#background')
            .fadeIn('slow', function() {
                spinner = new Spinner(opts).spin(bg);
            });

        setTimeout(function() {
            $body
                .find('#background')
                .fadeOut('slow', function() {
                    $(this).remove();
                });

            smoothScroll(target);

            spinner.stop();

            after();
        }, time * 1000);
    },

    /*
    * AJAX
    */
    // get page title using YQL 
    getPageTitle: function(url, callback) {
        var title = '';

        if (url.indexOf('http') > -1) {
            url = url.replace('http://', '');
            url = url.replace('https://', '');
        }
        
        $.ajax({
            type: "GET",
            url: "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%22http%3A%2F%2F" + url + "%22%20and%20xpath%3D'%2Fhtml%2Fhead%2Ftitle'%0A&format=json&diagnostics=true&callback=",
        }).success(function(data) {
            title = data.query.results.title;
            callback(title);
        }).error(function(error) {
            callback("(No se Encontro un Titulo de la Pagina, Escribe uno)");
        });
    },
    

    /*
    * Flesch Reading Ease
    */
    getFleschReadingEase: function(totalWords, totalSentences, totalSyllables) {
        var score = 206.835 - (1.015*totalWords)/totalSentences - (84.6*totalSyllables)/totalWords;

        return score.toFixed(2);
    },


    /* 
    * Keyword Density
    */
    getKeywordDensity: function(text, keyword) {
        var tkn = text.toLowerCase().replace(/\s\./g, '.').replace(/\s\,/g, '.').split(' ').length,
            nkr = h.getKeywordsOnString(text, keyword);

        return ((nkr/tkn)*100).toFixed(2);
    },

    
    /*
     * Check if there is any Images
     */
    getImagesLength: function(text) {
        var img = new RegExp('<img ', 'gi'),
            matches = text.match(img) || '';

        return matches.length;
    },


    /*
    * Text Manipulation
    */
    getSyllables: function(text) {
        if (text.length <= 3) {
            return 1;
        } else {
            return text.replace(/^y/, '').match(/[aeiou]{1,2}/g).length*0.92;
        }
    },

    getKeywordsOnString: function(text, keyword) {
        keyword = new RegExp(keyword, 'gi');
        matches = text.match(keyword) || '';

        return matches.length;
    },
    
    getTotalWords: function(text) {
        var totalWords = text.replace(/\s\./g, '.').replace(/\s\,/g, ',').split(' ').length;
        return totalWords;
    },

    getEndOfParagraph: function(text) {
        var endOfParagraph = text.split('^nbsp;') || '';
        return endOfParagraph.length;
    },

    getFirstParagraph: function(text) {
        var firstParagraph = text.match(/<p>.*<\/p>/gi)[0] || '';
        return firstParagraph;
    },

    getTotalSentences: function(text) {
        text = text || '';

        if (text.split('.').length > 1) {
            return text.match(/\.\s/g).length + 1;
        } else {
            return 1;
        }
    },

    getTextBold: function(text, keyword) {
        var boldText = text.replace(keyword, '<strong>' + keyword + '</strong>');
        return boldText;
    },

    capitalize: function(text) {
        var textCapitalized = text.replace(/^(.)|\s(.)/g, function($1) { return $1.toUpperCase() });
        return textCapitalized;
    },

    // Convert 'Something Like This' into this 'something-like-this'
    slugify: function(text) {
        var slugified = text.toLowerCase().replace(/\s/g, '-');

        // check if the last char is '-' and delete it 
        if (slugified.substr(-1) === '-') {
            slugified = slugified.substr(0, slugified.length -1);
        }

        return slugified;
   },

   /*
    * Score Messages
    */
    scores: {
        keywordDensity: {
            one: "Un poco bajo",
            two: "Excelente!",
            three: "Demasiado alta"
        },
        fleschReadingEase: {
            one: "Facil de leer",
            two: "Dificultad normal para leer el contenido",
            three: "Muy dificil de leer"
        },
        images: {
            one: "<p class='dark-grey'>Tu contenido contiene imagenes, muy bien!</p>",
            two: "<p class='dark-grey'>Tu contenito no contiene imagenes, no seria mala idea agregar por lo menos una</p>"

        },
        articleTitle: {
            one: "<p class='dark-grey'>El titulo contiene la palabra clave, Excelente!</p>",
            two: "<p class='dark-grey'>El titulo no contiene la palabra clave, no seria mala idea incluirla</p>",
            three: "<p class='dark-grey'>El titulo tiene mas de 40 caracteres y menos de 70, lo cual esta dentro de lo recomendado, muy bien!</p>",
            four: "<p class='dark-grey'>El titulo tiene menos de 30 caracteres, lo minimo recomendado es de 40</p>",
            five: "<p class='dark-grey'>El titulo tiene mas de 70 caracteres, lo maximo recomendado es de 70</p>"
        },
        keywordOnSubheadings: {
            one: "<p class='dark-grey'>La palabra clave esta en los sub titulos, muy bien</p>",
            two: "<p class='dark-grey'>La palabra clave no esta en los sub titulos, es buena idea incluirla en por lo menos una</p>"
        },
        keywordOnfirstParagraph: {
            one: "<p class='dark-grey'>La palabra clave aparece en el primer parrafo del contenido, muy bien!</p>",
            two: "<p class='dark-grey'>La palabra clave no aparece en el primer parrafo</p>"
        },
        keywordOnMetaDescription: {
            one: "<p class='dark-grey'>La palabra clave esta en la Meta Descripcion</p>",
            two: "<p class='dark-grey'>La palabra clave no aparece en la Meta Descripcion</p>"
        },
        wordsLength: {
            one: "<p class='dark-grey'> mas de 300, minimo recomendado, muy bien!</p>",
            two: "<p class='dark-grey'> y el minimo recomendado es de 300</p>"
        }
    }
}, editor = {};  

/*
 * CKEDITOR API
 */
CKEDITOR.on('instanceReady', function() {
    var instance = CKEDITOR.instances.editor1;

    editor.insertHtml = function(html) {
        instance.setData(html);
    };

    // get text from using ace api
    editor.getArticleText = function() {
        var aceText = instance.document.getBody().getText();
        return aceText;
    };

    // get html from article using ace api
    editor.getArticleHTML = function() {
        var aceHTML = instance.getData();
        return aceHTML;
    };
});
