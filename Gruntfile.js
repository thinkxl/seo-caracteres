var LIVERELOAD_PORT = 35729;
// var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

module.exports = function(grunt) {

    var siteConfig = {
        dev: 'dev',
        prod: 'prod'
    };
    
    // project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        site: siteConfig,

        watch: {
            options: {
                livereload: true,
                files: '*/*'
            },

            css: {
                files: '<%= site.dev %>/less/*.less',
                tasks: ['less']
            },

            livereload: {
                files: [
                    '<%= site.dev %>/*.html',
                    '<%= site.dev %>/js/*.js',
                    '<%= site.dev %>/js/*/*.js',
                    '<%= site.dev %>/css/*.css',
                    '<%= site.dev %>/images/*.{png, jpg, jpeg, gif}'
                ]
            }
        },
        
        less: {
            options: {
                syncImport: true,
            },
            target: {
                files: {
                    '<%= site.dev %>/css/style.css': '<%= site.dev %>/less/style.less'
                }
            }
        },

        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    cwd: '<%= site.dev %>/img',
                    src: '*.{png,jpg,jpeg}',
                    dest: '<%= site.prod %>/img'
                }]
            }
        },

        autoprefixer: {
            dist: {
                options: {
                    browsers: ['last 2 versions']
                },
                target: {
                    src: '<%= site.prod %>/css/style.min.css',
                    dest: '<%= site.prod %>/css/style.min.css'
                }
            }
        },


        useminPrepare: {
            options: {
                dest: '<%= site.prod %>'
            },
            html: '<%= site.dev %>/index.html'
        },

        usemin: {
            options: {
                dirs: ['<%= site.prod %>']
            },
            html: ['<%= site.prod %>/{,*/}*.html'],
            css: ['<%= site.prod %>/css/{,*/}*.css']
        },

        rsync: {
            options: {
                args: ['--verbose'],
                exclude: ['.git*', 'node_modules', 'models', 'views', 'collections', 'less', '*.md', '*~', '*.swp', '*.sh', '*.un~'],
                recursive: true
            },

            dist: {
                options: {
                    src: './<%= site.dev %>/',
                    dest: './<%= site.prod %>/'
                }
            }
        },

        connect: {
            options: {
                port: 9000,
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    middleware: function ( connect ) {
                        return [
                        mountFolder(connect, '.tmp'),
                        mountFolder(connect, '')
                        ];
                    }
                }
            }
        },

        open: {
            server: {
                path: 'http://localhost:<%= connect.options.port %>/dev'
            }
        }
    });

    // load plugins
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
   
    grunt.registerTask('server', function() {
        grunt.task.run([
            'connect:livereload',
            'open',
            'watch'
        ]);
    });

    // default task(s)
    grunt.registerTask('default', ['server']);

    // build
    grunt.registerTask('build', function() {
        grunt.task.run([
            'useminPrepare',
            'less',
            'rsync:dist',
            'usemin',
            'autoprefixer:dist',
            'imagemin:dist',
        ]);
    });

};
